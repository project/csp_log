<?php

namespace Drupal\csp_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Shows an aggregated overview of all CSP reports with dedicated logging.
 */
class LogOverviewForm extends FormBase {

  /**
   * The dedicated CSP logging service.
   *
   * @var \Drupal\csp_log\CspLogServiceInterface
   */
  protected $cspLog;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LogOverviewForm {
    $instance = parent::create($container);
    $instance->cspLog = $container->get('csp_log');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'csp_log_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Determine the current filters.
    $params = $this->getRequest()->query->all();
    $filters = [
      'search' => !empty($params['search']) ? $params['search'] : NULL,
      'type' => !empty($params['type']) ? $params['type'] : NULL,
      'date_from' => !empty($params['date_from']) ? $params['date_from'] : NULL,
      'date_to' => !empty($params['date_to']) ? $params['date_to'] : NULL,
    ];

    // Set up the filters fields.
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filters'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['form--inline']],
    ];
    $form['filters']['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#description' => $this->t('Search for a string in all fields of the reports'),
      '#default_value' => $filters['search'],
    ];
    $form['filters']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Log type'),
      '#options' => [
        'all' => $this->t('All log types'),
        'reportOnly' => $this->t('Report only'),
        'enforce' => $this->t('Enforced'),
      ],
      '#default_value' => $filters['type'] ?? 'all',
    ];
    $form['filters']['date_from'] = [
      '#type' => 'date',
      '#title' => $this->t('Date from'),
      '#default_value' => $filters['date_from'],
    ];
    $form['filters']['date_to'] = [
      '#type' => 'date',
      '#title' => $this->t('Date to'),
      '#default_value' => $filters['date_to'],
    ];

    // Add filter actions.
    $form['filters']['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-actions'],
      ],
      'filter' => [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#button_type' => 'primary',
        '#name' => 'filter',
      ],
      'reset' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#name' => 'reset',
      ],
    ];

    // Set up the table.
    $header = [
      'date' => [
        'data' => $this->t('Date'),
        'field' => 'timestamp',
        'sort' => 'desc',
      ],
      'document_uri' => [
        'data' => $this->t('Document URI'),
        'field' => 'document_uri',
      ],
      'referrer' => [
        'data' => $this->t('Referrer'),
        'field' => 'referrer',
      ],
      'blocked_uri' => [
        'data' => $this->t('Blocked URI'),
        'field' => 'blocked_uri',
      ],
      'effective_directive' => [
        'data' => $this->t('Effective directive'),
        'field' => 'effective_directive',
      ],
    ];
    $logs = $this->cspLog->fetchLogs($filters, 20, $header);
    $rows = [];
    foreach ($logs as $log) {
      $rows[] = [
        'date' => date('d/m/Y H:i', $log->timestamp),
        'document_uri' => $log->document_uri,
        'referrer' => $log->referrer,
        'blocked_uri' => $log->blocked_uri,
        'effective_directive' => $log->effective_directive,
      ];
    }
    $form['logs'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No dedicated CSP reporting logs found.'),
      '#attributes' => ['class' => ['csp-log-overview--table']],
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];

    $form['#attached']['library'][] = 'csp_log/overview';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Redirect with filters applied.
    $values = $form_state->getValues();
    $query = [
      'search' => !empty($values['search']) ? $values['search'] : NULL,
      'type' => !empty($values['type']) ? $values['type'] : NULL,
      'date_from' => !empty($values['date_from']) ? $values['date_from'] : NULL,
      'date_to' => !empty($values['date_to']) ? $values['date_to'] : NULL,
    ];
    $trigger = $form_state->getTriggeringElement();
    if ($trigger && !empty($trigger['#name']) && $trigger['#name'] === 'reset') {
      $query = [];
    }
    $url = Url::fromRoute('csp_log.overview');
    $url->setOption('query', $query);
    $form_state->setRedirectUrl($url);
  }

}
