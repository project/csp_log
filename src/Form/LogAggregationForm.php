<?php

namespace Drupal\csp_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Shows an aggregated overview of all CSP reports with dedicated logging.
 */
class LogAggregationForm extends FormBase {

  /**
   * The dedicated CSP logging service.
   *
   * @var \Drupal\csp_log\CspLogServiceInterface
   */
  protected $cspLog;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LogAggregationForm {
    $instance = parent::create($container);
    $instance->cspLog = $container->get('csp_log');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'csp_log_aggregation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Determine the current filters.
    $params = $this->getRequest()->query->all();
    $filters = [
      'search' => !empty($params['search']) ? $params['search'] : NULL,
      'type' => !empty($params['type']) ? $params['type'] : NULL,
      'date_from' => !empty($params['date_from']) ? $params['date_from'] : NULL,
      'date_to' => !empty($params['date_to']) ? $params['date_to'] : NULL,
      'min_amount' => !empty($params['min_amount']) ? $params['min_amount'] : NULL,
      'max_amount' => !empty($params['max_amount']) ? $params['max_amount'] : NULL,
    ];

    // Set up the filters fields.
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filters'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['form--inline']],
    ];
    $form['filters']['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#description' => $this->t('Search for a string in all fields of the reports'),
      '#default_value' => $filters['search'],
    ];
    $form['filters']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Log type'),
      '#options' => [
        'all' => $this->t('All log types'),
        'reportOnly' => $this->t('Report only'),
        'enforce' => $this->t('Enforced'),
      ],
      '#default_value' => $filters['type'] ?? 'all',
    ];
    $form['filters']['date_from'] = [
      '#type' => 'date',
      '#title' => $this->t('Date from'),
      '#default_value' => $filters['date_from'],
    ];
    $form['filters']['date_to'] = [
      '#type' => 'date',
      '#title' => $this->t('Date to'),
      '#default_value' => $filters['date_to'],
    ];
    $form['filters']['min_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Min. amount'),
      '#default_value' => $filters['min_amount'],
      '#min' => 0,
      '#step' => 1,
    ];
    $form['filters']['max_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. amount'),
      '#default_value' => $filters['max_amount'],
      '#min' => 0,
      '#step' => 1,
    ];

    // Add filter actions.
    $form['filters']['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-actions'],
      ],
      'filter' => [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#button_type' => 'primary',
        '#name' => 'filter',
      ],
      'reset' => [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#name' => 'reset',
      ],
    ];

    // Set up the table.
    $header = [
      'blocked_uri' => [
        'data' => $this->t('Blocked URI'),
        'field' => 'blocked_uri',
      ],
      'effective_directive' => [
        'data' => $this->t('Effective directive'),
        'field' => 'effective_directive',
      ],
      'amount' => [
        'data' => $this->t('Amount'),
        'field' => 'amount',
        'sort' => 'desc',
      ],
    ];
    $logs = $this->cspLog->aggregateLogs($filters, 20, $header);
    $rows = [];
    foreach ($logs as $log) {
      $rows[] = [
        'blocked_uri' => $log->blocked_uri,
        'effective_directive' => $log->effective_directive,
        'amount' => $log->amount,
      ];
    }
    $form['logs'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No dedicated CSP reporting logs found.'),
      '#attributes' => ['class' => ['csp-log-overview--table']],
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];

    $form['#attached']['library'][] = 'csp_log/overview';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Redirect with filters applied.
    $values = $form_state->getValues();
    $query = [
      'search' => !empty($values['search']) ? $values['search'] : NULL,
      'type' => !empty($values['type']) ? $values['type'] : NULL,
      'date_from' => !empty($values['date_from']) ? $values['date_from'] : NULL,
      'date_to' => !empty($values['date_to']) ? $values['date_to'] : NULL,
      'min_amount' => !empty($values['min_amount']) ? $values['min_amount'] : NULL,
      'max_amount' => !empty($values['max_amount']) ? $values['max_amount'] : NULL,
    ];
    $trigger = $form_state->getTriggeringElement();
    if ($trigger && !empty($trigger['#name']) && $trigger['#name'] === 'reset') {
      $query = [];
    }
    $url = Url::fromRoute('csp_log.aggregated');
    $url->setOption('query', $query);
    $form_state->setRedirectUrl($url);
  }

}
