<?php

namespace Drupal\csp_log;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Psr\Log\LoggerInterface;

/**
 * The service used to manage SFL functionality.
 */
class CspLogService implements CspLogServiceInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Creates a new CspLogService instance.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Logger channel.
   */
  public function __construct(
    Connection $database,
    LoggerInterface $logger,
  ) {
    $this->database = $database;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function insertLog(object $data, string $type) {
    // Check if we have the required fields.
    foreach (self::REQUIRED_REPORT_KEYS as $key) {
      if (empty($data->{$key})) {
        throw new \InvalidArgumentException("CSP report is missing required key: $key");
      }
    }

    // Insert the log.
    $fields = [
      'document_uri' => NULL,
      'effective_directive' => NULL,
      'blocked_uri' => NULL,
      'referrer' => NULL,
    ];
    foreach ($fields as $key => &$value) {
      $key = str_replace('_', '-', $key);
      if (!empty($data->{$key}) && is_string($data->{$key})) {
        $value = substr($data->{$key}, 0, 255);
      }
    }
    $fields['timestamp'] = time();
    $fields['report'] = json_encode($data, JSON_PRETTY_PRINT);
    $fields['type'] = $type === 'enforce' ? 'enforce' : 'reportOnly';
    $this->database->insert(self::DATABASE_TABLE)
      ->fields($fields)
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function fetchLogs(array $filters, int $amount = 0, array $sort = []): array {
    // Set up query base.
    $q = $this->database->select(self::DATABASE_TABLE, 't');
    $q->fields('t', [
      'id',
      'timestamp',
      'document_uri',
      'referrer',
      'blocked_uri',
      'effective_directive',
    ]);

    // Apply filters, pager and sort.
    $this->applyFilters($q, $filters);
    if ($amount) {
      $q = $q->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($amount);
    }
    if ($sort) {
      $q = $q->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($sort);
    }

    return $q->execute()->fetchAll();
  }

  /**
   * {@inheritDoc}
   */
  public function aggregateLogs(array $filters, int $amount = 0, array $sort = []): array {
    // Set up query base.
    $q = $this->database->select(self::DATABASE_TABLE, 't');
    $q->addField('t', 'blocked_uri');
    $q->addField('t', 'effective_directive');
    $q->addExpression('COUNT(t.id)', 'amount');
    $q->groupBy('t.blocked_uri');
    $q->groupBy('t.effective_directive');

    // Apply filters, pager and sort.
    $this->applyFilters($q, $filters);
    if ($amount) {
      $q = $q->extend('\Drupal\Core\Database\Query\PagerSelectExtender')->limit($amount);
    }
    if ($sort) {
      $q = $q->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($sort);
    }

    return $q->execute()->fetchAll();
  }

  /**
   * {@inheritDoc}
   */
  public function removeLogs(string $type, int $endDate, int $startDate = 0): int {
    return $this->database->delete(self::DATABASE_TABLE)
      ->condition('type', $type)
      ->condition('timestamp', "$endDate", '<=')
      ->condition('timestamp', "$startDate", '>=')
      ->execute();
  }

  /**
   * Helper function to apply filters to a select query.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $q
   *   The query.
   * @param array $filters
   *   The filters.
   */
  protected function applyFilters(SelectInterface &$q, array $filters) {
    if (!empty($filters['search'])) {
      $or = $this->database->condition('OR');
      $or->condition('t.document_uri', '%' . $this->database->escapeLike($filters['search']) . '%', 'LIKE');
      $or->condition('t.referrer', '%' . $this->database->escapeLike($filters['search']) . '%', 'LIKE');
      $or->condition('t.blocked_uri', '%' . $this->database->escapeLike($filters['search']) . '%', 'LIKE');
      $or->condition('t.effective_directive', '%' . $this->database->escapeLike($filters['search']) . '%', 'LIKE');
      $q->condition($or);
    }
    if (!empty($filters['type'])) {
      $type = $filters['type'] === 'enforce' ? 'enforce' : 'reportOnly';
      $q->condition('t.type', $type);
    }
    if (!empty($filters['date_from'])) {
      $q->condition('t.timestamp', (string) strtotime($filters['date_from']), '>=');
    }
    if (!empty($filters['date_to'])) {
      $q->condition('t.timestamp', (string) strtotime($filters['date_to']), '<=');
    }
    if (!empty($filters['min_amount'])) {
      $q->having('amount >= ' . $filters['min_amount']);
    }
    if (!empty($filters['max_amount'])) {
      $q->having('amount <= ' . $filters['date_to']);
    }
  }

}
