<?php

namespace Drupal\csp_log;

/**
 * Interface for CspLogServiceInterface.
 */
interface CspLogServiceInterface {

  /**
   * Name of the database table used to save the logs.
   */
  const DATABASE_TABLE = 'csp_log';

  /**
   * A list of keys that are required for reports to be logged.
   */
  const REQUIRED_REPORT_KEYS = [
    'document-uri',
    'effective-directive',
    'blocked-uri',
  ];

  /**
   * Inserts a new CSP report log.
   *
   * @param object $data
   *   The report data.
   * @param string $type
   *   The report type.
   *
   * @throws \InvalidArgumentException
   * @throws \Exception
   */
  public function insertLog(object $data, string $type);

  /**
   * Fetches all CSP report logs matching the given filters.
   *
   * @param array $filters
   *   The filters to apply.
   * @param int $amount
   *   The amount of logs to fetch (per page).
   * @param array $sort
   *   The sorting array for tableSort.
   *
   * @return array
   *   An array of logs.
   */
  public function fetchLogs(array $filters, int $amount = 0, array $sort = []): array;

  /**
   * Fetches ann aggregation of CSP report logs matching the given filters.
   *
   * @param array $filters
   *   The filters to apply.
   * @param int $amount
   *   The amount of logs to fetch (per page).
   * @param array $sort
   *   The sorting array for tableSort.
   *
   * @return array
   *   An array of aggregated log data.
   */
  public function aggregateLogs(array $filters, int $amount = 0, array $sort = []): array;

  /**
   * Remove all logs between a start and end date.
   *
   * @param string $type
   *   The report type.
   * @param int $endDate
   *   The end date, as a unix timestamp.
   * @param int $startDate
   *   The start date, as a unix timestamp.
   *
   * @return int
   *   Number of deleted logs.
   */
  public function removeLogs(string $type, int $endDate, int $startDate = 0): int;

}
