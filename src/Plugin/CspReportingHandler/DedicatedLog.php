<?php

namespace Drupal\csp_log\Plugin\CspReportingHandler;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\csp\Csp;
use Drupal\csp\Plugin\ReportingHandlerBase;

/**
 * Csp Reporting Handler to use a dedicated log provided by csp_log.
 *
 * @CspReportingHandler(
 *   id = "csp_log",
 *   label = @Translation("Dedicated CSP log"),
 *   description = @Translation("Reports will be added to a dedicated log, provided by the csp_log module."),
 * )
 */
class DedicatedLog extends ReportingHandlerBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form): array {
    $form['cleanup'] = [
      '#type' => 'number',
      '#title' => $this->t('Log lifetime (days)'),
      '#description' => $this->t('Enter the number of days to keep logs before removing them. Enter "0" to keep logs indefinitely.'),
      '#default_value' => $this->configuration['cleanup'] ?? 0,
      '#min' => 0,
      '#step' => 1,
      '#states' => [
        'required' => [
          ':input[name="' . $this->configuration['type'] . '[enable]"]' => ['checked' => TRUE],
          ':input[name="' . $this->configuration['type'] . '[reporting][handler]"]' => ['value' => $this->pluginId],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterPolicy(Csp $policy): void {
    $reportUri = Url::fromRoute(
      'csp_log.reporturi',
      ['type' => ($this->configuration['type'] == 'enforce') ? 'enforce' : 'reportOnly'],
      ['absolute' => TRUE]
    );
    $policy->setDirective('report-uri', $reportUri->toString());
  }

}
