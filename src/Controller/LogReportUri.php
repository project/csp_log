<?php

namespace Drupal\csp_log\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\csp_log\CspLogServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Report URI Controller.
 *
 * @package Drupal\csp_log\Controller
 */
class LogReportUri implements ContainerInjectionInterface {

  /**
   * The Request Stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The dedicated CSP logging service.
   *
   * @var \Drupal\csp_log\CspLogServiceInterface
   */
  protected $cspLog;

  /**
   * Create a new Log Report URI Controller.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The Request Stack service.
   * @param \Drupal\csp_log\CspLogServiceInterface $cspLog
   *   The dedicated CSP logging service.
   */
  public function __construct(RequestStack $requestStack, CspLogServiceInterface $cspLog) {
    $this->requestStack = $requestStack;
    $this->cspLog = $cspLog;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('csp_log')
    );
  }

  /**
   * Handle a report submission.
   *
   * @param string $type
   *   The report type.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An empty response.
   */
  public function log(string $type) {
    $reportJson = $this->requestStack->getCurrentRequest()->getContent();
    $report = json_decode($reportJson);
    $report = !empty($report->{'csp-report'}) ? $report->{'csp-report'} : $report;

    // Return 400: Bad Request if content cannot be parsed.
    if (empty($report) || json_last_error() != JSON_ERROR_NONE) {
      return new Response('', 400);
    }

    // Try to create a log, returning error responses if needed.
    try {
      $this->cspLog->insertLog($report, $type);

      // 202: Accepted.
      return new Response('', 202);
    }
    catch (\InvalidArgumentException) {
      // Return 400: Bad Request if we're missing vital parts of the report.
      return new Response('', 400);
    }
    catch (\Exception) {
      return new Response('', 500);
    }
  }

}
