# CSP logging

## Background

At the time of this module's creation, there was no way in either the csp 
module or the seckit module to have a CSP reporting endpoint that does not 
crowd the site logs without creating a custom route. This module provides such 
a route. Additionally, it provides a CspReportingHandler for the csp module.

## How to use it

* Install as you would any other module.
* Configure your CSP header using /log-report-uri/report-only or 
/log-report-uri/enforce as a report uri.
  * Using the CSP module, this can be done by selecting the "Dedicated CSP log"
plugin for either the enforce or report only settings.
* Check /admin/reports/csp or /admin/reports/csp/aggregated-logs to see the CSP
reports or aggregation.
